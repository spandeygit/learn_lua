# /cygdrive/c/Program\ Files/wireshark/editcap -T user0 -F pcap gtpc0.pcap gtpc0_147.pcap
import sys
#!{sys.executable} -m pip install dpkt
#!pip install dpkt
#!type python
import dpkt

# CREATE DLT u0
f2 = open('./heur.pcap', 'wb')
pcapw = dpkt.pcap.Writer(f2)


time = 31.001
for i in range(0,2):
    my_ba = bytearray(1074 + 5); 
    # MAC 
    my_ba[0] = 0xda;my_ba[1] = 0xda;my_ba[2] = 0xda;my_ba[3] = 0xda;my_ba[4] = 0xda;my_ba[5] = 0xda;
    # MAC 
    my_ba[6] = 0xaa;  my_ba[7] = 01;  my_ba[8] = 0x23;my_ba[9] = 0x45;  my_ba[10] = 0x67;my_ba[11] = 0x89; 
    # ETHER
    my_ba[12] = 0x08;my_ba[13] = 0x00;


    #-------------------------------------
    # IP address header 
    # IP version / IHL 0x45
    my_ba[14] = 0x45;

    # IP DSCP
    my_ba[15] = 3;


    # IP Length
    my_ba[16] = 0x04;my_ba[17] = 0x24;
    # IP ID 
    my_ba[18] = 0x55; my_ba[19] = 0xe0;

    # flags and frag offset 
    my_ba[20] = 0x00; 
    my_ba[21] = 0x00; 


    # TTL
    my_ba[22] = 0x00;

    # Protocol 
    my_ba[23] = 17;
    # checksum 
    my_ba[24] = 0xad; my_ba[25] = 0xfa;


    #  SRC IP 
    my_ba[26] = 205;
    my_ba[27] = 206;
    my_ba[28] = 0x00;
    my_ba[29] = 0x00;

    #  DST IP 
    my_ba[30] = 205;
    my_ba[31] = 206;
    my_ba[32] = 0x00;
    my_ba[33] = 0x00;

    #-------------------------------------
    # UDP 
    #  SRC PORT
    my_ba[34] = 0x00;
    my_ba[35] = 0x05;

    #  DST PORT
    my_ba[36] = 205;
    my_ba[37] = 206;


    #  UDP LENGTH
    my_ba[38] = 0x4;
    my_ba[39] = 0x10;

    #  UDP CHECKSUM
    my_ba[40] = 0x00;
    my_ba[41] = 0x04;
    #-------------------------------------
    my_ba[1074] = 0x07;
    my_ba[1075] = 0x06;
    my_ba[1076] = 0x05;
    my_ba[1077] = 0xbe;
    my_ba[1078] = 0xef;


    time = time + 0.001; # add 1 us
    pcapw.writepkt(my_ba, time);

pcapw.close()
