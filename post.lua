awsome_protocol = Proto("AWSOME", "AWSOME PROTOCOL") 
D1 = ProtoField.string("AWSOME.Summary", "Summary")
awsome_protocol.fields = {D1}
local IPSRC_GLBL = Field.new("ip.src");


-- POST DISSECTOR 
function awsome_protocol.dissector(buffer, pinfo, tree)
    length = buffer:len();
    pinfo.cols.protocol = awsome_protocol.name
    local subtree = tree:add(awsome_protocol,buffer(), "AWSOME PROTOCOL SUMMARY")
    local ipsrc_fetch = IPSRC_GLBL()
	local ip_tvb = ipsrc_fetch.range()
	subtree:add(D1, buffer(), tostring(ip_tvb(0, 1):uint()))
end

register_postdissector(awsome_protocol)