--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <https://www.gnu.org/licenses/>.

local proto_FOO = Proto("simple-post","Add data to info")

local FOO = Field.new("ip.proto");
local sum = 0;
local mytable = {}

function proto_FOO.dissector(tvb,pinfo,tree)
    local t = FOO()
    if (t == nil) then 
       return
    end

    info(mytable[pinfo.number])

    if (mytable[pinfo.number] ~= nil) then
        return
    end


    mytable[pinfo.number] = true;

    local ts = tostring(t);
    local tsi = tonumber(ts);
    sum = sum + tsi;
    info("myproto dissector called here" .. tostring(sum) .. " and proto = " .. tsi);
    pinfo.cols.info:append("PROTO --> " .. ts .."---- called (times)  : " ..  tostring(sum));
    file = io.open("debug.txt", "a")
    io.output(file)
    io.write("PROTO --> " .. ts .."---- called (times)  : " ..  tostring(sum))
    io.close(file)


end


register_postdissector(proto_FOO)
