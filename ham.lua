--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <https://www.gnu.org/licenses/>.


-- User defined link-layers are referenced in Wireshark within the range:
--  147-162 DLT_USER0-DLT_USER15.


ham_protocol = Proto("ham", "ham_protocol") 
SRC = ProtoField.uint32("HAM.SRC", "SRC", base.HEX)
DST = ProtoField.uint32("HAM.DST", "DST", base.HEX)
TYPE = ProtoField.uint16("HAM.TYPE", "TYPE", base.HEX)
ham_protocol.fields = {SRC,DST, TYPE}




-- LINK LAYER - HAM
function ham_protocol.dissector(buffer, pinfo, tree)
    length = buffer:len();
    pinfo.cols.protocol = ham_protocol.name
    local subtree = tree:add(ham_protocol,buffer(), "HAM PROTOCOL TREE")
    local eth_withoutfcs = Dissector.get("eth_withoutfcs")
    -- local eth_withoutfcs = Dissector.get("eth_withoutfcs")
    subtree:add( SRC, buffer(0,4))
    subtree:add( DST, buffer(4,4))
    subtree:add( TYPE, buffer(8,2))
    eth_withoutfcs(buffer(10, length - 10):tvb(), pinfo, tree)
end


