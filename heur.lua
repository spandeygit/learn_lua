--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <https://www.gnu.org/licenses/>.


heur_protocol = Proto("HEUR", "HEUR_TRAILER")

foo = ProtoField.uint8 ("heur_protocol.FOO", "FOO", base.HEX)
bar = ProtoField.uint16("heur_protocol.BAR", "BAR", base.HEX)
magic = ProtoField.uint16("heur_protocol.MAGIC", "MAGIC", base.HEX)

heur_protocol.fields = {foo,bar, magic}

local function exist_checker(buffer, pinfo, tree)
    length = buffer:len()
    --if length < 5 then return false end
    local potential_magicnum = buffer(length-2,2):uint()
    if potential_magicnum == 0xbeef then 
        heur_protocol.dissector(buffer, pinfo, tree)
        return true 
    end
    return false
end

function heur_protocol.dissector(buffer, pinfo, tree)
    length = buffer:len()
    pinfo.cols.protocol = heur_protocol.name
    local subtree = tree:add(heur_protocol, buffer(), "HEUR TRAILER")
    subtree:add(foo, buffer(length-3,1), buffer(length-3,1):uint());
    subtree:add(bar, buffer(length-5,2), buffer(length-5,2):uint());
    subtree:add(magic, buffer(length-2,2), buffer(length-2,2):uint());
end

heur_protocol:register_heuristic("eth.trailer", exist_checker)
